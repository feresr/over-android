package app.over.android.interview

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition
import androidx.test.espresso.intent.Intents.intended
import androidx.test.espresso.intent.matcher.IntentMatchers.hasComponent
import androidx.test.espresso.intent.rule.IntentsTestRule
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import app.over.android.interview.ColorAdapter.ColorViewHolder
import app.over.android.interview.view.ColorInfoActivity
import app.over.android.interview.view.PalettePickerActivity
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@LargeTest
class PalettePickerActivityEspressoTest {

    @get:Rule
    val activityRule = IntentsTestRule(PalettePickerActivity::class.java)

    @Test
    fun Add_Color_tap_opens_ColorInfoActivity() {
        onView(withId(R.id.buttonAddColor)).perform(click())
        onView(withId(R.id.recyclerView)).perform(
            actionOnItemAtPosition<ColorViewHolder>(0, click())
        )
        intended(hasComponent(ColorInfoActivity::class.java.name))
    }

}
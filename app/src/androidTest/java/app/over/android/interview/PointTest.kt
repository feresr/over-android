package app.over.android.interview

import android.graphics.Matrix
import androidx.test.ext.junit.runners.AndroidJUnit4
import app.over.android.interview.model.Point
import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class PointTest {

    @Test
    fun pointTranslation() {
        // given
        val matrix = Matrix()
        val point = Point(5.0f, 0.0f)

        // when
        matrix.preTranslate(10f, 0f)
        val newPoint = point * matrix

        // then
        assertEquals(Point(15.0f, 0.0f), newPoint)
    }

    @Test
    fun pointScale() {
        // given
        val matrix = Matrix()
        val point = Point(1.0f, 2.0f)

        // when
        matrix.preScale(2f, 10f)
        val newPoint = point * matrix

        // then
        assertEquals(Point(2.0f, 20.0f), newPoint)
    }
}

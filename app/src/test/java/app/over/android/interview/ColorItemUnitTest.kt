package app.over.android.interview

import app.over.android.interview.model.ColorItem
import org.junit.Assert.assertEquals
import org.junit.Test

class ColorItemUnitTest {

    @Test
    fun `complementary simple`() {
        val item = ColorItem(0x030BFF)
        val complementary = item.complementary
        assertEquals(0xFCF400, complementary.colorInt)
    }

    @Test
    fun `second complementary should be equal to the original color`() {
        val original = ColorItem(0xAA11FF)
        val complementary = original.complementary.complementary
        assertEquals(original, complementary)
    }

    @Test
    fun `ColorItem#toHexString simple`() {
        val item = ColorItem(0x0000FF)
        val expected = item.toHexString()
        assertEquals("0000FF", expected)
    }

    @Test
    fun `ColorItem#toHexString with prefix argument`() {
        val item = ColorItem(0xFF0000)
        val expected = item.toHexString('X')
        assertEquals("XFF0000", expected)
    }

    @Test
    fun `ColorItem#toHexString explicit null prefix`() {
        val item = ColorItem(0x00FFAA)
        val expected = item.toHexString(null)
        assertEquals("00FFAA", expected)
    }
}

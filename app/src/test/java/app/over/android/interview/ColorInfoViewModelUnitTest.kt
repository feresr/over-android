package app.over.android.interview

import app.over.android.interview.ColorInfoViewModel.ColorInfoState
import app.over.android.interview.model.ColorInfo
import app.over.android.interview.model.ColorItem
import app.over.android.interview.utils.Logger
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.kotlin.any
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever

@ExperimentalCoroutinesApi
class ColorInfoViewModelUnitTest {

    private val repository: ColorInfoRepository = mock()
    private val logger: Logger = mock()
    private val viewModel: ColorInfoViewModel = ColorInfoViewModel(repository, logger)

    private val testCoroutineDispatcher = TestCoroutineDispatcher()

    @Before
    fun setup() {
        Dispatchers.setMain(testCoroutineDispatcher)
    }

    @Test
    fun `initial view state is View-Loading`() = runBlockingTest {
        // given
        val colors = mutableListOf<ColorInfoState>()

        // when
        val job = launch { viewModel.colorInfo.toList(colors) }

        // then
        assertEquals(ColorInfoState.Loading, colors[0])

        job.cancel()
    }

    @Test
    fun `successful ColorInfo emits a View-Success state`() = runBlockingTest {
        // given
        val colors = mutableListOf<ColorInfoState>()
        val response: ColorInfo = mock()
        whenever(repository.getColorInfo(any())).thenReturn(response)

        // when
        val job = launch { viewModel.colorInfo.toList(colors) }
        viewModel.getColorInfo(ColorItem(0XFF0000))

        // then
        assertEquals(ColorInfoState.Loading, colors[0])
        assertEquals(ColorInfoState.Success(response), colors[1])

        job.cancel()
    }

    @Test
    fun `unsuccessful ColorInfo request emits a View-Error state`() = runBlockingTest {
        // given
        val colors = mutableListOf<ColorInfoState>()
        val errorMessage = "Error Message"
        whenever(repository.getColorInfo(any())).thenThrow(RuntimeException(errorMessage))

        // when
        val job = launch { viewModel.colorInfo.toList(colors) }
        viewModel.getColorInfo(ColorItem(0XFF0000))

        // then
        assertEquals(ColorInfoState.Loading, colors[0])
        assertEquals(ColorInfoState.Error, colors[1])

        job.cancel()
    }
}
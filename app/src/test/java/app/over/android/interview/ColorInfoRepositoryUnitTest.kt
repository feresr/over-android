package app.over.android.interview

import app.over.android.interview.model.CMYK
import app.over.android.interview.model.ColorItem
import app.over.android.interview.model.HSV
import app.over.android.interview.model.RGB
import app.over.android.interview.network.ColorInfoClient
import app.over.android.interview.network.ColorResponseProto
import com.google.gson.Gson
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert.assertEquals
import org.junit.Test
import org.mockito.kotlin.mock
import org.mockito.kotlin.times
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever
import java.io.InputStreamReader

@ExperimentalCoroutinesApi
class ColorInfoRepositoryUnitTest {

    private val colorInfoClient: ColorInfoClient = mock()
    private val colorInfoRepository = ColorInfoRepository(colorInfoClient)

    @Test
    fun `correct mapping of ColorResponseProto to ColorInfo`() = runBlockingTest {
        // given
        val inputStream = javaClass.classLoader?.getResourceAsStream("FF8777.json")
        val reader = InputStreamReader(inputStream)

        val colorResponseProto = reader.use { Gson().fromJson(it, ColorResponseProto::class.java) }
        val colorItem = ColorItem(0xFF8777)
        whenever(colorInfoClient.fetchColor("FF8777")).thenReturn(colorResponseProto)

        //when
        val colorInfo = colorInfoRepository.getColorInfo(colorItem)

        //then
        assertEquals("FF8777", colorInfo.hex)
        assertEquals("Bittersweet", colorInfo.name)
        assertEquals(ColorItem(0xFF8777), colorInfo.item)
        assertEquals(RGB(255u, 135u, 119u), colorInfo.rgb)
        assertEquals(HSV(7u, 53u, 100u), colorInfo.hsv)
        assertEquals(CMYK(0u, 47u, 53u, 0u), colorInfo.cmyk)
    }

    @Test
    fun `correct mapping of ColorResponseProto with no name to ColorInfo`() = runBlockingTest {
        // given
        val inputStream = javaClass.classLoader?.getResourceAsStream("FF8777-no-name.json")
        val reader = InputStreamReader(inputStream)

        val colorResponseProto = reader.use { Gson().fromJson(it, ColorResponseProto::class.java) }
        val colorItem = ColorItem(0xFF8777)
        whenever(colorInfoClient.fetchColor("FF8777")).thenReturn(colorResponseProto)

        //when
        val colorInfo = colorInfoRepository.getColorInfo(colorItem)

        //then
        assertEquals("FF8777", colorInfo.hex)
        assertEquals(null, colorInfo.name)
        assertEquals(ColorItem(0xFF8777), colorInfo.item)
        assertEquals(RGB(255u, 135u, 119u), colorInfo.rgb)
        assertEquals(HSV(7u, 53u, 100u), colorInfo.hsv)
        assertEquals(CMYK(0u, 47u, 53u, 0u), colorInfo.cmyk)
    }

    @Test
    fun `cache should be used for two or more identical color info requests`() = runBlockingTest {
        // given
        val inputStream = javaClass.classLoader?.getResourceAsStream("FF8777.json")
        val reader = InputStreamReader(inputStream)

        val colorResponseProto = reader.use { Gson().fromJson(it, ColorResponseProto::class.java) }
        val colorItem = ColorItem(0xFF8777)
        whenever(colorInfoClient.fetchColor("FF8777")).thenReturn(colorResponseProto)

        //when
        colorInfoRepository.getColorInfo(colorItem)
        colorInfoRepository.getColorInfo(colorItem)
        colorInfoRepository.getColorInfo(colorItem)

        //then
        verify(colorInfoClient, times(1)).fetchColor("FF8777")
    }
}
package app.over.android.interview

import app.over.android.interview.model.ColorItem
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert.assertEquals
import org.junit.Test

@ExperimentalCoroutinesApi
class PalettePickerViewModelUnitTest {

    private val viewModel = PalettePickerViewModel()

    @Test
    fun `do not allow for duplicate colors`() = runBlockingTest {
        // given
        val colors = mutableListOf<List<ColorItem>>()

        // when
        val job = launch { viewModel.colorList.toList(colors) }
        viewModel.addColor(0xFF0000)
        viewModel.addColor(0xFF0000)
        viewModel.addColor(0xFF0000)

        // then
        assertEquals(emptyList<ColorItem>(), colors[0])
        assertEquals(listOf(ColorItem(0xFF0000)), colors[1])
        assertEquals(2, colors.size)

        job.cancel()
    }

    @Test
    fun `new colors should be added at the start of the list`() = runBlockingTest {
        // given
        val colors = mutableListOf<List<ColorItem>>()

        // when
        val job = launch { viewModel.colorList.toList(colors) }
        viewModel.addColor(0xFF0000)
        viewModel.addColor(0x00FF00)
        viewModel.addColor(0x0000FF)

        // then
        assertEquals(
            emptyList<ColorItem>(),
            colors[0]
        )

        assertEquals(
            listOf(ColorItem(0xFF0000)),
            colors[1]
        )

        assertEquals(
            listOf(
                ColorItem(0x00FF00),
                ColorItem(0xFF0000)
            ), colors[2]
        )

        assertEquals(
            listOf(
                ColorItem(0x0000FF),
                ColorItem(0x00FF00),
                ColorItem(0xFF0000)
            ),
            colors[3]
        )

        job.cancel()
    }
}
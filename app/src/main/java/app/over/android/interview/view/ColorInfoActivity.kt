package app.over.android.interview.view

import android.content.ClipData
import android.content.ClipboardManager
import android.graphics.Color
import android.os.Bundle
import android.widget.Toast.LENGTH_SHORT
import android.widget.Toast.makeText
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import app.over.android.interview.ColorInfoViewModel
import app.over.android.interview.R
import app.over.android.interview.databinding.ActivityColorInfoBinding
import app.over.android.interview.ColorInfoViewModel.ColorInfoState
import app.over.android.interview.ColorInfoViewModel.ColorInfoState.Error
import app.over.android.interview.ColorInfoViewModel.ColorInfoState.Loading
import app.over.android.interview.ColorInfoViewModel.ColorInfoState.Success
import app.over.android.interview.model.ColorInfo
import app.over.android.interview.model.ColorItem
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class ColorInfoActivity : AppCompatActivity() {

    private lateinit var binding: ActivityColorInfoBinding
    private val viewModel: ColorInfoViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityColorInfoBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val color = ColorItem(intent.getIntExtra(ARG_COLOR, -1))
        init(color)
        viewModel.getColorInfo(color)
    }

    private fun init(color: ColorItem) {
        val colorHex = color.toHexString('#')
        binding.header.button.text = colorHex
        title = colorHex
        binding.header.colorPreview.background.setTint(Color.parseColor(colorHex))

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.elevation = 0f

        lifecycleScope.launch {
            lifecycle.repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.colorInfo.collect { state -> bindState(state) }
            }
        }
    }

    private fun bindState(state: ColorInfoState) {
        with(binding) {
            complementary.root.isVisible = state is Success
            rgb.root.isVisible = state is Success
            hsv.root.isVisible = state is Success
            cmyk.root.isVisible = state is Success
            progressIndicator.isVisible = state is Loading
        }

        if (state is Success) bindColor(state.colorInfo)
        if (state is Error) makeText(this, R.string.generic_error, LENGTH_SHORT).show()
    }

    private fun bindColor(color: ColorInfo) {
        title = color.name ?: color.hex

        with(binding.header) {
            button.setOnClickListener { addToClipboard("#${color.hex}") }
            colorPreview.background.setTint(Color.parseColor("#${color.hex}"))
        }

        with(binding.rgb) {
            val (r, g, b) = color.rgb
            redLabel.value = r.toString()
            greenLabel.value = g.toString()
            blueLabel.value = b.toString()
        }

        with(binding.hsv) {
            val (h, s, v) = color.hsv
            hueLabel.value = h.toString()
            saturationLabel.value = s.toString()
            valueLabel.value = v.toString()
        }

        with(binding.cmyk) {
            val (c, m, y, k) = color.cmyk
            cyanLabel.value = c.toString()
            magentaLabel.value = m.toString()
            yellowLabel.value = y.toString()
            blackLabel.value = k.toString()
        }

        val complementary = color.item.complementary.toHexString('#')
        binding.complementary.label.text = complementary
        binding.complementary.thumbnail.background.setTint(Color.parseColor(complementary))
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return super.onSupportNavigateUp()
    }

    private fun addToClipboard(color: String) {
        val clipboard: ClipboardManager = getSystemService(CLIPBOARD_SERVICE) as ClipboardManager
        val clip = ClipData.newPlainText("Color", color)
        clipboard.setPrimaryClip(clip)
        makeText(this, "Copied to Clipboard!", LENGTH_SHORT).show()
    }

    companion object {
        const val ARG_COLOR = "arg_color"
    }
}
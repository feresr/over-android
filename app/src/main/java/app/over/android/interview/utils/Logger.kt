package app.over.android.interview.utils

import android.util.Log

interface Logger {
    fun log(message: String)
}

class SimpleLogger : Logger {
    override fun log(message: String) {
        Log.e(this::class.simpleName, message)
    }
}
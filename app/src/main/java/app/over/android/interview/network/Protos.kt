package app.over.android.interview.network

data class NameProto(val value: String?)
data class RGBProto(val r: UByte, val g: UByte, val b: UByte)
data class HSVProto(val h: UByte, val s: UByte, val v: UByte)
data class CMYKProto(val c: UByte, val m: UByte, val y: UByte, val k: UByte)
data class HexProto(val value: String, val clean: String)

data class ColorResponseProto(
    val name: NameProto?,
    val hex: HexProto,
    val rgb: RGBProto,
    val hsv: HSVProto,
    val cmyk: CMYKProto,
)
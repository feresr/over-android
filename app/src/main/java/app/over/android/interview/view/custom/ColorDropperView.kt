package app.over.android.interview.view.custom

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapShader
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Matrix
import android.graphics.Paint
import android.graphics.RectF
import android.graphics.Shader.TileMode.CLAMP
import android.util.AttributeSet
import android.view.GestureDetector.OnGestureListener
import android.view.MotionEvent
import android.view.View
import androidx.core.view.GestureDetectorCompat
import app.over.android.interview.R
import app.over.android.interview.model.Point
import app.over.android.interview.utils.contains
import app.over.android.interview.utils.get
import com.almeros.android.multitouch.MoveGestureDetector
import com.almeros.android.multitouch.MoveGestureDetector.OnMoveGestureListener
import kotlin.math.abs

class ColorDropperView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr), OnMoveGestureListener, OnGestureListener {

    var currentColor: Int = Color.WHITE
        private set

    private var dropperPoint: Point? = null

    private var backingBitmap: Bitmap? = null

    private val backingBitmapRect: RectF = RectF(0.0f, 0.0f, 0.0f, 0.0f)
        get() {
            field.right = backingBitmap?.width?.toFloat() ?: 0f
            field.bottom = backingBitmap?.height?.toFloat() ?: 0f
            return field
        }

    private val viewRect = RectF(0f, 0f, 0f, 0f)
        get() {
            field.right = width.toFloat()
            field.bottom = height.toFloat()
            return field
        }

    private val drawingMatrix: Matrix = Matrix()

    private val inverseDrawingMatrix: Matrix = Matrix()
        get() {
            drawingMatrix.invert(field)
            return field
        }

    private val innerCircleBitmapPaint: Paint = Paint(Paint.ANTI_ALIAS_FLAG)

    private val moveGestureDetector: MoveGestureDetector = MoveGestureDetector(context, this)
    private val gestureDetector: GestureDetectorCompat = GestureDetectorCompat(context, this)

    private var moveGestureDelta = 0f

    private val outerCirclePaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        style = Paint.Style.STROKE
        color = Color.WHITE
        strokeWidth = OUTSIDE_CIRCLE_SIZE
    }

    private val whiteCircleOuter = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        style = Paint.Style.STROKE
        color = Color.WHITE
        strokeWidth = context
            .resources
            .getDimensionPixelSize(R.dimen.stroke_width_mask_pointer).toFloat()
    }

    private val pixelPointRect = RectF()

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        val point = dropperPoint ?: return
        canvas.apply {
            if (backingBitmap != null) {
                drawCircle(point.x, point.y, SIZE_OF_MAGNIFIER, innerCircleBitmapPaint)
            }
            drawRect(pixelPointRect, whiteCircleOuter)
            drawCircle(
                point.x, point.y, SIZE_OF_MAGNIFIER + OUTSIDE_CIRCLE_SIZE / 2, outerCirclePaint
            )
            drawCircle(
                point.x, point.y, SIZE_OF_MAGNIFIER + OUTSIDE_CIRCLE_SIZE, whiteCircleOuter
            )
        }
    }

    fun setBackingBitmap(original: Bitmap) {
        val bitmap = original.copy(original.config, true).also { backingBitmap = it }
        val canvas = Canvas(bitmap)
        val paint = Paint().apply {
            color = Color.WHITE
            style = Paint.Style.STROKE
            strokeWidth = 2f
        }
        canvas.drawRect(canvas.clipBounds, paint)

        innerCircleBitmapPaint.shader = BitmapShader(bitmap, CLAMP, CLAMP)
        dropperPoint?.let { point -> updateZoomedBitmap(point) } ?: invalidate()
    }

    private fun setPoint(point: Point) {
        this.dropperPoint = point

        with(pixelPointRect) {
            left = point.x - PIXEL_POINT_SQUARE_SIZE
            top = point.y - PIXEL_POINT_SQUARE_SIZE
            right = point.x + PIXEL_POINT_SQUARE_SIZE
            bottom = point.y + PIXEL_POINT_SQUARE_SIZE
        }

        updateZoomedBitmap(point)
    }

    private fun updateZoomedBitmap(point: Point) {
        val bitmap = backingBitmap ?: return
        drawingMatrix.reset()

        // Translate the bitmap to the center of the view
        drawingMatrix.postTranslate((width - bitmap.width) / 2f, (height - bitmap.height) / 2f)

        // Scale the bitmap up or down to fit the view, respecting its original aspect ratio
        drawingMatrix.setRectToRect(backingBitmapRect, viewRect, Matrix.ScaleToFit.CENTER)

        // Move the dropper point from view-space coordinates to bitmap-space coordinates
        val bitmapSpacePoint = point * inverseDrawingMatrix
        currentColor = if (bitmapSpacePoint in bitmap) bitmap[bitmapSpacePoint] else Color.WHITE
        outerCirclePaint.color = currentColor

        // Zoom in the bitmap by MAGNIFIER_SCALE, with the view-space coordinates point as pivot
        drawingMatrix.postScale(MAGNIFIER_SCALE, MAGNIFIER_SCALE, point.x, point.y)
        innerCircleBitmapPaint.shader?.setLocalMatrix(drawingMatrix)

        invalidate()
    }

    private fun moveColorDropperPoint(translateX: Float, translateY: Float) {
        val point = dropperPoint ?: Point(width / 2f, height / 2f)
        setPoint(point + Point(translateX, translateY))
    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        event?.apply {
            when (action) {
                MotionEvent.ACTION_UP, MotionEvent.ACTION_CANCEL -> {
                    moveGestureDelta = 0f
                }
            }
            return moveGestureDetector.onTouchEvent(event).or(gestureDetector.onTouchEvent(event))
        }
        return false
    }

    override fun onMoveBegin(detector: MoveGestureDetector?): Boolean {
        return true
    }

    override fun onMove(detector: MoveGestureDetector?): Boolean {
        detector?.let { gesture ->
            val moveThresholdReached = abs(moveGestureDelta) >= MOVE_THRESHOLD
            if (!moveThresholdReached) {
                moveGestureDelta += gesture.focusDelta.length()
            }
            if (moveThresholdReached && gesture.focusDelta.length() != 0f) {
                moveColorDropperPoint(gesture.focusDelta.x, gesture.focusDelta.y)
            }
            return true
        }
        return false
    }

    @SuppressLint("DrawAllocation") // ok, should only happen the very first time
    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        super.onLayout(changed, left, top, right, bottom)
        if (dropperPoint == null) setPoint(Point(width / 2f, height / 2f))
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        backingBitmap = null
        dropperPoint = null
    }

    override fun onMoveEnd(detector: MoveGestureDetector?) {}

    override fun onShowPress(e: MotionEvent?) {}

    override fun onSingleTapUp(event: MotionEvent?): Boolean {
        if (event == null) return false
        setPoint(Point(event.x, event.y))
        return true
    }

    override fun onDown(e: MotionEvent?): Boolean = true

    override fun onFling(
        e1: MotionEvent?, e2: MotionEvent?, velocityX: Float, velocityY: Float
    ): Boolean = false

    override fun onScroll(
        e1: MotionEvent?, e2: MotionEvent?, distanceX: Float, distanceY: Float
    ): Boolean = false

    override fun onLongPress(e: MotionEvent?) {}

    companion object {
        private const val SIZE_OF_MAGNIFIER = 120f
        private const val PIXEL_POINT_SQUARE_SIZE = 10
        private const val MAGNIFIER_SCALE = 3f
        private const val OUTSIDE_CIRCLE_SIZE = 30f
        private const val MOVE_THRESHOLD = 20f
    }
}
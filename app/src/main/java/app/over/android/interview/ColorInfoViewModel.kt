package app.over.android.interview

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import app.over.android.interview.di.DependencyProvider
import app.over.android.interview.model.ColorInfo
import app.over.android.interview.model.ColorItem
import app.over.android.interview.utils.Logger
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

class ColorInfoViewModel(
    private val colorInfoRepository: ColorInfoRepository = DependencyProvider.repository,
    private val logger: Logger = DependencyProvider.logger
) : ViewModel() {

    private val _coloInfo = MutableStateFlow<ColorInfoState>(ColorInfoState.Loading)
    val colorInfo: StateFlow<ColorInfoState> = _coloInfo

    fun getColorInfo(color: ColorItem) = viewModelScope.launch {
        try {
            val colorInfo = colorInfoRepository.getColorInfo(color)
            _coloInfo.value = ColorInfoState.Success(colorInfo)
        } catch (e: Exception) {
            logger.log(e.toString())
            _coloInfo.value = ColorInfoState.Error
        }
    }

    sealed class ColorInfoState {
        object Loading : ColorInfoState()
        data class Success(val colorInfo: ColorInfo) : ColorInfoState()
        object Error : ColorInfoState()
    }
}
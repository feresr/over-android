package app.over.android.interview.view

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.doOnLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle.State.STARTED
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager.HORIZONTAL
import app.over.android.interview.R
import app.over.android.interview.databinding.FragmentPaletteBinding
import app.over.android.interview.model.ColorItem
import app.over.android.interview.ColorAdapter
import app.over.android.interview.PalettePickerViewModel
import app.over.android.interview.utils.BitmapRequestListener
import com.bumptech.glide.Glide
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class PalettePickerFragment : Fragment() {

    private val viewModel by lazy { ViewModelProvider(this)[PalettePickerViewModel::class.java] }
    private val adapter = ColorAdapter(::onColorTapped)

    lateinit var binding: FragmentPaletteBinding

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewLifecycleOwner.lifecycleScope.launch {
            viewLifecycleOwner.repeatOnLifecycle(STARTED) {
                viewModel.colorList.collect { colors ->
                    binding.recyclerView.scrollToPosition(0)
                    adapter.update(colors)
                }
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentPaletteBinding.inflate(inflater, container, false)
        init()
        return binding.root
    }

    private fun init() {
        binding.imageViewPalette.doOnLayout {
            Glide.with(this)
                .asBitmap()
                .load(R.drawable.sample_image)
                .listener(BitmapRequestListener(binding.colorDropperView::setBackingBitmap))
                .submit(
                    binding.imageViewPalette.width,
                    binding.imageViewPalette.height
                )
        }

        binding.recyclerView.layoutManager = LinearLayoutManager(context, HORIZONTAL, false)
        binding.recyclerView.adapter = adapter

        binding.buttonAddColor.setOnClickListener {
            viewModel.addColor(binding.colorDropperView.currentColor)
        }
    }

    private fun onColorTapped(color: ColorItem) {
        val intent = Intent(context, ColorInfoActivity::class.java)
        intent.putExtra(ColorInfoActivity.ARG_COLOR, color.colorInt)
        startActivity(intent)
    }
}
package app.over.android.interview.di

import app.over.android.interview.ColorInfoRepository
import app.over.android.interview.network.ColorInfoClient
import app.over.android.interview.utils.Logger
import app.over.android.interview.utils.SimpleLogger

/**
 * This class provides minimal dependency injection solution.
 * Replacing this with Dagger or Hilt would be a great idea!
 */
object DependencyProvider {
    private val client: ColorInfoClient = ColorInfoClient()
    val repository: ColorInfoRepository = ColorInfoRepository(client)
    val logger: Logger = SimpleLogger()
}
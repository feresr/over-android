package app.over.android.interview

import app.over.android.interview.model.CMYK
import app.over.android.interview.model.ColorInfo
import app.over.android.interview.model.ColorItem
import app.over.android.interview.model.HSV
import app.over.android.interview.model.RGB
import app.over.android.interview.network.ColorInfoClient

class ColorInfoRepository(private val client: ColorInfoClient) {

    private val memoryCache: MutableMap<Int, ColorInfo> = mutableMapOf()

    suspend fun getColorInfo(color: ColorItem): ColorInfo {
        val cached = memoryCache[color.colorInt]
        if (cached != null) return cached

        val colorResponse = client.fetchColor(color.toHexString())
        val (r, g, b) = colorResponse.rgb
        val (h, s, v) = colorResponse.hsv
        val (c, m, y, k) = colorResponse.cmyk

        val network = ColorInfo(
            item = color,
            hex = colorResponse.hex.clean,
            name = colorResponse.name?.value,
            rgb = RGB(r, g, b),
            hsv = HSV(h, s, v),
            cmyk = CMYK(c, m, y, k)
        )

        memoryCache[color.colorInt] = network
        return network
    }
}

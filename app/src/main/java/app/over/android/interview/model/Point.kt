package app.over.android.interview.model

import android.graphics.Matrix

data class Point(val x: Float, val y: Float) {
    operator fun plus(point: Point): Point {
        return Point(this.x + point.x, this.y + point.y)
    }

    operator fun times(matrix: Matrix): Point {
        val coordinates = floatArrayOf(x, y)
        matrix.mapPoints(coordinates)
        return Point(coordinates[0], coordinates[1])
    }
}
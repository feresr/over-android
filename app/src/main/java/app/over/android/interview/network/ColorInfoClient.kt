package app.over.android.interview.network

import io.ktor.client.HttpClient
import io.ktor.client.engine.cio.CIO
import io.ktor.client.features.json.GsonSerializer
import io.ktor.client.features.json.JsonFeature
import io.ktor.client.features.logging.ANDROID
import io.ktor.client.features.logging.LogLevel
import io.ktor.client.features.logging.Logger
import io.ktor.client.features.logging.Logging
import io.ktor.client.request.get
import io.ktor.http.ContentType
import io.ktor.http.contentType

class ColorInfoClient {

    private val client = HttpClient(CIO) {
        install(Logging) {
            logger = Logger.ANDROID
            level = LogLevel.HEADERS
        }
        install(JsonFeature) { serializer = GsonSerializer() }
    }

    suspend fun fetchColor(hex: String): ColorResponseProto {
        return client.get("$URL/id?hex=$hex") { contentType(ContentType.Application.Json) }
    }

    companion object {
        const val URL = "https://www.thecolorapi.com"
    }
}
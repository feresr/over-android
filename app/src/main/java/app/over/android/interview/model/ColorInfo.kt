package app.over.android.interview.model

data class RGB(val r: UByte, val g: UByte, val b: UByte)
data class HSV(val h: UByte, val s: UByte, val v: UByte)
data class CMYK(val c: UByte, val m: UByte, val y: UByte, val k: UByte)

data class ColorInfo(
    val item: ColorItem,
    val hex: String,
    val name: String?,
    val rgb: RGB,
    val hsv: HSV,
    val cmyk: CMYK,
)

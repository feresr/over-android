package app.over.android.interview

import androidx.lifecycle.ViewModel
import app.over.android.interview.model.ColorItem
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow

class PalettePickerViewModel : ViewModel() {

    private val _colorList = MutableStateFlow<List<ColorItem>>(emptyList())
    val colorList: StateFlow<List<ColorItem>> = _colorList

    fun addColor(color: Int) {
        val newColor = ColorItem(color)
        _colorList.value = (setOf(newColor) + _colorList.value).toList()
    }
}
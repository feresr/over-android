package app.over.android.interview.model

data class ColorItem(val colorInt: Int) {

    val complementary: ColorItem
        get() {
            val complementary = colorInt xor 0xFFFFFF
            return ColorItem(complementary)
        }

    fun toHexString(prefix: Char? = null): String {
        val format = if (prefix == null) "%06X" else "$prefix%06X"
        return String.format(format, 0xFFFFFF and colorInt)
    }
}
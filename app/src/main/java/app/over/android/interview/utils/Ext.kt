package app.over.android.interview.utils

import android.graphics.Bitmap
import app.over.android.interview.model.Point
import kotlin.math.roundToInt

operator fun Bitmap.contains(point: Point): Boolean {
    return point.x.roundToInt() in 0 until width && point.y.roundToInt() in 0 until height
}

operator fun Bitmap.get(point: Point): Int {
    return getPixel(point.x.roundToInt(), point.y.roundToInt())
}
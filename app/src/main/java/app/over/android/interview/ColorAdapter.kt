package app.over.android.interview

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView.Adapter
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import app.over.android.interview.databinding.ListItemColorBinding
import app.over.android.interview.model.ColorItem
import app.over.android.interview.ColorAdapter.ColorViewHolder

class ColorAdapter(private val onItemSelected: (ColorItem) -> Unit) : Adapter<ColorViewHolder>() {

    var items: List<ColorItem> = emptyList()
        private set

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ColorViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ListItemColorBinding.inflate(inflater, parent, false)
        return ColorViewHolder(binding)
    }

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: ColorViewHolder, position: Int) {
        val item = items[position]
        holder.bind(item)
    }

    fun update(newItems: List<ColorItem>) {
        val diff = DiffUtil.calculateDiff(DiffCallback(newItems))
        this.items = newItems
        diff.dispatchUpdatesTo(this)
    }

    inner class ColorViewHolder(private val binding: ListItemColorBinding) :
        ViewHolder(binding.root) {

        fun bind(colorItem: ColorItem) {
            binding.colorItemView.background.setTint(colorItem.colorInt)
            itemView.setOnClickListener { onItemSelected(colorItem) }
        }
    }

    inner class DiffCallback(private val newItems: List<ColorItem>) : DiffUtil.Callback() {
        override fun getOldListSize(): Int = items.size

        override fun getNewListSize(): Int = newItems.size

        override fun areItemsTheSame(
            oldItemPosition: Int, newItemPosition: Int
        ): Boolean = newItems[newItemPosition].colorInt == items[oldItemPosition].colorInt

        override fun areContentsTheSame(
            oldItemPosition: Int, newItemPosition: Int
        ): Boolean = newItems[newItemPosition] == items[oldItemPosition]
    }
}


package app.over.android.interview.view.custom

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.LinearLayout
import app.over.android.interview.R
import app.over.android.interview.databinding.ViewColorLabelBinding

class ColorLabel @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : LinearLayout(context, attrs, defStyleAttr) {

    private val binding = ViewColorLabelBinding.inflate(
        LayoutInflater.from(context), this
    )

    private var label: CharSequence
        get() = binding.label.text
        set(value) {
            binding.label.text = value
        }

    var value: CharSequence
        get() = binding.value.text
        set(value) {
            binding.value.text = value
        }

    init {
        orientation = HORIZONTAL

        context.theme.obtainStyledAttributes(attrs, R.styleable.ColorLabel, 0, 0).apply {
            try {
                label = getString(R.styleable.ColorLabel_label) ?: ""
                value = getString(R.styleable.ColorLabel_value) ?: ""
            } finally {
                recycle()
            }
        }
    }
}
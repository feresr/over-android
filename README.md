# Android Interview Technical Assessment 🤖 

Thank you so much for applying to Over to be a part of the Android team.
We ask that you complete this programming challenge, for us to get a better understanding of your work.

## The Problem 🕵
The application is a color palette app. You are able to pick a color from an image, and then save these colors to a palette. 
You are given this incomplete/buggy application, that needs to be cleaned up, bugs fixed and a new feature implemented in it.

### Things to do:
- Fix as many of these bugs as you can:
    - The new color is added to the end of the list instead of at the beginning.
    - The list of colors for the text is showing the colors repeated in it.
    - The `ColorDropperView` isn't properly displaying the inner zoomed in bitmap (it seems that the offset is wrong). 
      When hovering over a certain section of the bitmap, the zoomed in bitmap is showing a different section of the underlying bitmap. This bug is the most difficult to solve in this assessment and is considered a bonus question. If you are applying for a role that involves graphics, we would like to see this bug resolved.
    - Sometimes the color picked up by the color dropper adds incorrectly - ie adds white when the color of the dropper behind is blue
    - There is no animation when adding a new color to the list (nice to have)
    
- Clean up the code base to follow some kind of architecture (there is currently no structure/architecture to anything)
- Add the following feature: When clicking on a color, more information needs to be loaded into the `ColorInfoActivity` (More information below)

## Color Information Feature 🎨
When clicking on a color on the bottom `RecyclerView`, connect to the following API to get more information about the color: (https://www.thecolorapi.com/)
https://www.thecolorapi.com/id?hex=24B1E0

Please display the following information on the screen:
- If the color has a name
- The Hex Color
- The RGB Values
- The HSV Values
- The CMYK Values
- The complementary color (not from the API)

The different color values should also be able to be copied to the clipboard.

Here is a rough idea of how the app should look, use this as a guide:

| Main Screen | Color Details |
| ------ | ----- |
| <img src="art/main_screen.png"/> | <img src="art/detail_screen.png"/> |

    
## Submission Requirements ✅

It is up to you to solve this problem using any technology/libraries you want to use. We recommend following good coding principles and architecture, as well as implementing tests.

Please submit your solution within 5 days of receiving the test. If you need more time, then do let us know. 
You can use either a Google Drive or Dropbox link or a private Github/Gitlab link to submit your work. 
Please include a write up of the changes you made and improvements that should be made, if you had more time to work on the app.

If you are struggling with any of the requirements or if something is not clear, please don't hesitate to reach out. 

Good luck!

--

### Hi there! 👋 

**I had a lot of fun working on this exercise, Thanks for the opportunity!**

###  My changes: 💻

- Applied Kotlin style guides (ktlint)
- Refactored the project to use a minimal MVVM architecture.
- Re organised classes to have fields at the top.
- Added visibility modifiers, removed redundant fields and potentially un-safe nullable `!!` calls.
- Removed duplicate colours from the palette. See: `PalettePickerViewModel.kt`
- Delayed bitmap loading until the view has been measured & laid out.
(The original implementation was calling `Glide.subimt(w, h)` too early when `[measured]Width/Height` were still `0`)
- Fixed the offset in the inner zoomed preview by translating and scaling the matrix accordingly.
- Tried to reduce the number of allocations that happened during frequently called methods like `moveColorDropperPoint`
- Made use of `DiffUtils` to update only the relevant portions of the recycler-view. This not only improves performance but it also gives us animations! 
- Added a simple memory cache to avoid identical request to the server (this in turn improves UX)
- Created Protos (or Data transfer objects) to decouple the server response from app internal business logic.
- Removed Kotlin Synthetics in favor of ViewBinding (I love how convenient `Kotlin Synthetics` are! but using ViewBinding is the safer/recommended approach nowadays)
- Implemented clip to clipboard feature request
- Wrote tests

### Improvements I'd like to make: ✨

- Configure pro-guard (and make sure the Protos don't get obfuscated!)
- Refactor `ColorInfoRepository`. This class is perhaps doing too many things at the moment.
Networking + Parsing + Caching. I would like to split it up into an interface and add support for different data sources (Network, Memory, Disk. etc)
- Add better error handling. Right now I'm showing a very generic error when something goes wrong, would be nice to tell apart Network issues from backend problems, etc.
- Related to the above, I'd like to make possible to re-try a request if/when an error happens (pull to refresh).
- Add more Tests
- Add a DI library like Dagger/Hilt (I'm using DI already, but doing so manually)

| My Main Screen | My Color Details Screen |
| ------ | ----- |
| <img src="art/FR-main.png"/> | <img src="art/FR-detail.png"/> |